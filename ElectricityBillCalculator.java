package LO3;

import java.util.Scanner;


public class ElectricityBillCalculator {

	public static void main(String[] args) {
		
		int x = 0;
		while (x == 0) {
			Scanner scanner = new Scanner(System.in);
			System.out.println("\nEnter option type: \n" + "1 - Adding a new Account \n" + "2 - Delete an Account \n"
					+ "3 - Add number of unit for a month \n" + "4 - Find average bill amount per month\n"
					+ "5 - Find total bill amount for the year\n" + "6 - Search an account\n" + "7 - End \n");
			int option = scanner.nextInt();

			switch (option) {
			case 1:
				Scanner scanner1 = new Scanner(System.in);
				System.out.println("Enter your Account id");
				int accountID = scanner1.nextInt();

				System.out.println("Enter your Account name");
				String accountName = scanner1.next();

//			System.out.println("Enter number of units for every month\n sample format space seperated : 3 5 6 9 12 34 2 23 9 6 34 12");
//			String accountUnits =(new Scanner(System.in)).nextLine();
//			System.out.println(accountUnits);
//			System.out.println((accountUnits.split(" "))[2]);
//			int[] unitArr = Stream.of((accountUnits.split(" "))).mapToInt(Integer::parseInt).toArray();
				// System.out.println(unitArr[11]);

				ElectricityAccount ea = new ElectricityAccount();
				ea.setAccountID(accountID);
				ea.setAccountName(accountName);
				// ea.setUnitArr(unitArr);

				// System.out.println(unitArr[5]);

				ElectricityAccountDaoImpl eadl = new ElectricityAccountDaoImpl();
				eadl.addElectricityAccount(ea);
				break;

			case 2:
				Scanner scanner2 = new Scanner(System.in);
				System.out.println("Enter your Account id");
				int accountID2 = scanner2.nextInt();

				ElectricityAccountDaoImpl eadl2 = new ElectricityAccountDaoImpl();
				eadl2.deleteElectricityAccount(accountID2);
				break;
			case 3:
				Scanner scanner3 = new Scanner(System.in);
				System.out.println("Enter your Account id");
				int accountID3 = scanner3.nextInt();

				System.out.println("Enter the month number");
				int monthNum = scanner3.nextInt();

				System.out.println("Enter number of units");
				int numOfUnits = scanner3.nextInt();

				ElectricityAccountDaoImpl eadl3 = new ElectricityAccountDaoImpl();
				eadl3.addNumberOfUnitsForMonth(accountID3, monthNum, numOfUnits);
				break;
			case 4:
				Scanner scanner4 = new Scanner(System.in);
				System.out.println("Enter your Account id");
				int accountID4 = scanner4.nextInt();

				System.out.println("Enter the month number");
				int monthNum2 = scanner4.nextInt();

				ElectricityAccountDaoImpl eadl4 = new ElectricityAccountDaoImpl();
				System.out.println("Amount for the month " + monthNum2 + " is : "
						+ eadl4.findBillForTheMonth(accountID4, monthNum2));
				break;
			case 5:
				Scanner scanner5 = new Scanner(System.in);
				System.out.println("Enter your Account id");
				int accountID5 = scanner5.nextInt();

				ElectricityAccountDaoImpl eadl5 = new ElectricityAccountDaoImpl();
				eadl5.findBillForTheYear(accountID5);
				break;
			case 6:
				Scanner scanner6 = new Scanner(System.in);
				System.out.println("Enter your Account id");
				int accountID6 = scanner6.nextInt();

				ElectricityAccountDaoImpl eadl6 = new ElectricityAccountDaoImpl();
				eadl6.searchAnAccount(accountID6);
				break;
			case 7:
				x = 1;
				break;
			default:
				break;
			}
		}
		System.out.println("\n Program ended....\n");
	}

}
