package LO3;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DataSource {
	public Connection createConnection() throws SQLException {
		Connection con=DriverManager.getConnection("jdbc:mysql://localhost:3307/electricity_bill_db","root","1234");
		return con;		
	}
}
