package LO3;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.mail.search.AndTerm;


public class ElectricityAccountDaoImpl implements ElectricityAccountDao {
    Connection dbConnection = null;
    PreparedStatement statement = null;
    
	@Override
	public void addElectricityAccount(ElectricityAccount electricityAccount) {

	    String sql = "insert into table1(accountID,accountName,m1,m2,m3,m4,m5,m6,m7,m8,m9,m10,m11,m12) values(?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
	//    String sql = "insert into table1 values("+electricityAccount.getAccountID() +" , "+ "'" +electricityAccount.getAccountName()+"'"+",?,?,?)";
	    DataSource dataSource = new DataSource();
	    try {
			dbConnection = dataSource.createConnection();
			statement = dbConnection.prepareStatement(sql);
			statement.setInt(1, electricityAccount.getAccountID());
			statement.setString(2, electricityAccount.getAccountName());
			for (int i=3; i<15; i++) {
				statement.setInt(i, (electricityAccount.getUnitArr())[i-3]);
			}
			int rowCount=statement.executeUpdate();
		    
		   // for (int i=0; i<3; i++) {
//		    	String sql1 = "insert into table1(m1_2020) values(?)";
//		    	PreparedStatement statement2 = dataSource.createConnection().prepareStatement(sql1);
//		    	statement2.setInt(3, 1);
		 //   }
		    System.out.println("Acoount Updated Successfully......!");
		    
		    
		} catch (SQLException e) {
			e.printStackTrace();
		}
	    
	}

	@Override
	public void deleteElectricityAccount(int accountID) {

	    
	    String sql = "delete from table1 where accountID="+ accountID;
		
	    DataSource dataSource = new DataSource();
	    try {
			dbConnection = dataSource.createConnection();
		    statement = dbConnection.prepareStatement(sql);
		    statement.executeUpdate(sql);

		    System.out.println("Record is deleted from table for accountID : "+ accountID);
		} catch (SQLException e) {
			e.printStackTrace();
		}

	}

	@Override
	public void addNumberOfUnitsForMonth(int accountID, int monthNum, int numOfUnits) {

	    	    
		String sql ="update table1 set m"+monthNum+"="+ numOfUnits + " where accountID="+ accountID;
		
		DataSource dataSource = new DataSource();
		try {
			dbConnection = dataSource.createConnection();
		    statement = dbConnection.prepareStatement(sql);
		    statement.executeUpdate(sql);
		    System.out.println("Units Updated Successfully......!");
		} catch (SQLException e) {
			e.printStackTrace();
		}

	}

	@Override
	public void findBillForTheYear(int accountID) {
		double total = 0;
		for (int i = 1; i <= 12; i++) {
			total += findBillForTheMonth(accountID, i);
		}
		System.out.println("Total amount for the year is : "+total);
		
	}

	@Override
	public double findBillForTheMonth(int accountID, int monthNum) {
	    Connection dbConnection = null;
	    Statement statement = null;
	    double amount = 0;
	    
	    String sql = "SELECT m"+monthNum+" from table1 WHERE accountID = "+accountID;
	    //String sql = "SELECT m"+monthNum+" from table1 WHERE accountID = "+accountID;

		DataSource dataSource = new DataSource();
		
		try {
			dbConnection = dataSource.createConnection();
			statement = dbConnection.createStatement();
			ResultSet rs = statement.executeQuery(sql);
			
			while(rs.next()) {
				String mon = "m"+monthNum;
				int unit = rs.getInt(mon);
				if(unit<90) {
					amount = 10 * unit;
				}else if (90<=unit && unit <=120) {
					amount = 15 * unit;
				}else if (unit > 129) {
					amount = 20 * unit;
				}
			}

			rs.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return amount;
		
	}

	@Override
	public void searchAnAccount(int accountID) {
	    Connection dbConnection = null;
	    Statement statement = null;
	    
	    String sql = "SELECT accountID, accountName, m1,m2,m3,m4 from table1";
		DataSource dataSource = new DataSource();
		try {
			dbConnection = dataSource.createConnection();
			statement = dbConnection.createStatement();
			ResultSet rs = statement.executeQuery(sql);
			
			while(rs.next()) {
				if ((rs.getInt("accountID"))==(accountID)){
					System.out.println("your account id : "+accountID);
					System.out.println("your account name is : "+rs.getString("accountName"));
				}
			}
			rs.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

}
