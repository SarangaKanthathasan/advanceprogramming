package LO3;

public interface ElectricityAccountDao {
	public void addElectricityAccount(ElectricityAccount electricityAccount);
	public void deleteElectricityAccount(int accountID);
	public void addNumberOfUnitsForMonth(int accountID, int monthNum, int numOfUnits);
	public void findBillForTheYear(int accountID);
	public double findBillForTheMonth(int accountID, int monthNum);
	public void searchAnAccount(int accountID);
}
