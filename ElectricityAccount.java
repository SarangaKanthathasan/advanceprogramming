package LO3;


public class ElectricityAccount {
	private int accountID;
	private String accountName;
	private int[] unitArr = new int[12];
	
	public int getAccountID() {
		return accountID;
	}
	public void setAccountID(int accountID) {
		this.accountID = accountID;
	}
	public String getAccountName() {
		return accountName;
	}
	public void setAccountName(String accountName) {
		this.accountName = accountName;
	}
	public int[] getUnitArr() {
		return unitArr;
	}
	public void setUnitArr(int[] unitArr) {
		this.unitArr = unitArr;
	}
	
}
